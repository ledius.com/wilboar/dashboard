import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './@core/components/navbar/navbar.component';
import { DrawerComponent } from './@core/components/drawer/drawer.component';
import { NotFoundComponent } from './@core/components/not-found/not-found.component';
import { FooterComponent } from './@core/components/footer/footer.component';
import { PageComponent } from './@core/components/page/page.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DrawerComponent,
    NotFoundComponent,
    FooterComponent,
    PageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
